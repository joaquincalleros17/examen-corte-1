package com.example.cotizacion_preexamen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class CotizacionActivity extends AppCompatActivity {
    private Nomina nomina;

    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;

    private EditText txtNumero;
    private EditText txtNombre;
    private EditText txtHoras;
    private EditText txtExtras;
    private RadioButton radio1;
    private RadioButton radio2;
    private RadioButton radio3;
    private TextView lblSubtotal;
    private TextView lblImpuesto;
    private TextView lblTotal;

    private String descripcion;
    private float valorAuto;
    private float enganche;
    private int puesto;
    private float pagoMensual;
    private int numero=0;
    private int horas=0;
    private int extras=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cotizacion);
        txtNumero = (EditText)findViewById(R.id.txtNumero);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtHoras = (EditText)findViewById(R.id.txtHoras);
        txtExtras = (EditText)findViewById(R.id.txtExtras);
        radio1 = (RadioButton) findViewById(R.id.radio1);
        radio2 = (RadioButton) findViewById(R.id.radio2);
        radio3 = (RadioButton) findViewById(R.id.radio3);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);

        lblSubtotal = (TextView) findViewById(R.id.lblsubtotal);
        lblImpuesto = (TextView) findViewById(R.id.lblImpuesto);
        lblTotal = (TextView)findViewById(R.id.lblTotal);

        Bundle datos = getIntent().getExtras();
        txtNombre.setText( datos.getString("cliente"));
        nomina = (Nomina) datos.getSerializable("nomina");


        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!txtNumero.getText().toString().isEmpty()&&!txtExtras.getText().toString().isEmpty() && !txtNombre.getText().toString().isEmpty() && !txtHoras.getText().toString().isEmpty()) {
                    puesto = 0;
                    if (radio1.isChecked()) {
                        puesto = 1;
                    } else if (radio2.isChecked()) {
                        puesto = 2;
                    } else if (radio3.isChecked()) {
                        puesto = 3;
                    }
                    nomina.setNombre(txtNombre.getText().toString());
                    nomina.setNumero(Integer.valueOf(txtNumero.getText().toString()));
                    nomina.setHoras(Integer.valueOf(txtHoras.getText().toString()));
                    nomina.setHorasEx(Integer.valueOf(txtExtras.getText().toString()));
                    nomina.setPuesto(puesto);
                    lblSubtotal.setText("Subtotal: $" + String.valueOf(nomina.subtotal()));
                    lblImpuesto.setText("Impuesto: $" + String.valueOf(nomina.impuesto()));
                    lblTotal.setText("Total: $" + String.valueOf(nomina.total()));
                } else {
                    Toast.makeText(CotizacionActivity.this, "Llene todos los campos", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtNumero.setText("");
                txtNombre.setText("");
                txtHoras.setText("");
                txtExtras.setText("");
                lblSubtotal.setText("Subtotal: $");
                lblImpuesto.setText("Impuesto: $");
                lblTotal.setText("Total: $");
                radio1.setChecked(true);
            }
        });
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
