package com.example.cotizacion_preexamen;

import java.io.Serializable;
import java.util.Random;

public class Cotizacion implements Serializable {
    private int folio;
    private String descripcion;
    private float valorAuto;
    private float porEnganche;
    private int plazos;

    public Cotizacion() {
        this.folio = this.generarFolio();
    }

    public Cotizacion(int folio, String descripcion, float valorAuto, float porEnganche, int plazos) {
        this.folio = this.generarFolio();
        this.descripcion = descripcion;
        this.valorAuto = valorAuto;
        this.porEnganche = porEnganche;
        this.plazos = plazos;
    }

    public int getFolio() {
        return folio;
    }

    public void setFolio(int folio) {
        this.folio = folio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getValorAuto() {
        return valorAuto;
    }

    public void setValorAuto(float valorAuto) {
        this.valorAuto = valorAuto;
    }

    public float getPorEnganche() {
        return porEnganche;
    }

    public void setPorEnganche(float porEnganche) {
        this.porEnganche = porEnganche;
    }

    public int getPlazos() {
        return plazos;
    }

    public void setPlazos(int plazos) {
        this.plazos = plazos;
    }

    //Comportamiento
    public int generarFolio(){
        return new Random().nextInt()%1000;
    }
    public float calcularEnganche(){
        return this.valorAuto * (this.porEnganche/100);
    }
    public float pagoMensual(){
        float enganche = this.calcularEnganche();
        float totalFin = this.valorAuto - enganche;
        return totalFin / this.plazos;
    }
}
