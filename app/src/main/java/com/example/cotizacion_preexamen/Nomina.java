package com.example.cotizacion_preexamen;

import java.io.Serializable;

public class Nomina implements Serializable {
    private int numero;
    private String nombre;
    private int horas;
    private int horasEx;
    private int puesto;

    public Nomina() {
    }

    public Nomina(int numero, String nombre, int horas, int horasEx, int puesto) {
        this.numero = numero;
        this.nombre = nombre;
        this.horas = horas;
        this.horasEx = horasEx;
        this.puesto = puesto;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getHoras() {
        return horas;
    }

    public void setHoras(int horas) {
        this.horas = horas;
    }

    public int getHorasEx() {
        return horasEx;
    }

    public void setHorasEx(int horasEx) {
        this.horasEx = horasEx;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }
    public float subtotal(){
        float sub =0;
        if(puesto == 1){
            sub = horas * 50 + horasEx*100;
        }
        if(puesto == 2){
            sub = horas * 70 + horasEx*140;
        }
        if(puesto == 3){
            sub = horas * 100 + horasEx*200;
        }
        return sub;
    }
    public float impuesto(){
        return subtotal() *0.16f;
    }
    public float total(){
        return subtotal() - impuesto();
    }
}
